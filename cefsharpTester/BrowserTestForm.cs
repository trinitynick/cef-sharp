﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace cefsharpTester
{
    public partial class BrowserTestForm : DevExpress.XtraEditors.XtraForm
    {
        public BrowserTestForm()
        {
            InitializeComponent();

            var defaultURL = "google.com";

            // NOTE: Unable to drag the browser control onto the form. I beleive this is expected. 
            var browser = new BrowserControl(defaultURL)
            {
                Dock = DockStyle.Fill
            };
            this.Controls.Add(browser);
        }
    }
}