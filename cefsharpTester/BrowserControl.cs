﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using CefSharp.WinForms;
using CefSharp;

namespace cefsharpTester
{
    public partial class BrowserControl : DevExpress.XtraEditors.XtraUserControl
    {
        protected ChromiumWebBrowser _browser;
        protected bool _toolBarEnabled;

        /// <summary>
        /// Callback to enable/disable toolbar buttons based on the state of the browser
        /// </summary>
        delegate void SetToolBarCallback();

        /// <summary>
        /// Callback to sync the URL text box with address of browser 
        /// </summary>
        /// <param name="address"></param>
        delegate void SetAddressCallback(string address);

        /// <summary>
        /// Default constructor (ToolBar ENABLED)
        /// </summary>
        /// <param name="defaultURL">Default web URL</param>
        public BrowserControl(string defaultURL) : this(defaultURL, true)
        {
        }

        /// <summary>
        /// Initialize new instance of Browser Control and explicitly set enable state of browser toolbar 
        /// </summary>
        /// <param name="defaultURL">Default web URL</param>
        /// <param name="toolbBarEnabled">ToolBar enabled</param>
        public BrowserControl(string defaultURL, bool toolbBarEnabled)
        {
            InitializeComponent();
            // note: order matters
            _toolBarEnabled = toolbBarEnabled;
            InitializeBrowser(defaultURL);
            SetToolBarEnabledState();
        }

        /// <summary>
        /// Browser page title changed event
        /// </summary>
        public event EventHandler<BrowserChangedEventArgs> TitleChanged;

        /// <summary>
        /// Browser document/page is finished loading
        /// NOTE: custom event args are not necessary for prototype (at the moment)
        /// </summary>
        public event EventHandler<EventArgs> PageLoaded;

        /// <summary>
        /// Initialize Cef Settings Browser object
        /// Cef.Initialize() can only be called ONCE. Comment from project contributor: You can only initialize CEF once, per application. It's just how CEF functions.
        /// </summary>
        /// <param name="defaultURL"></param>
        public void InitializeBrowser(string defaultURL)
        {
            // CEF can only be initialized once per process. 
            // Perferences can be set in CustomRequestContextHandler (not implmeneted yet)
            if (!Cef.IsInitialized)
            {
                var settings = new CefSettings
                {
                    BrowserSubprocessPath = @"x86\CefSharp.BrowserSubprocess.exe"
                };

                Cef.Initialize(settings, performDependencyCheck: false, browserProcessHandler: null);
            }

            _browser = new ChromiumWebBrowser(defaultURL);
            _browser.TitleChanged += Browser_TitleChanged;
            _browser.LoadingStateChanged += Browser_LoadingStateChanged;
            _browser.AddressChanged += Browser_AddressChanged;
            
            toolStripContainer1.ContentPanel.Controls.Add(_browser);
            txtUrl.Text = defaultURL;
        }

        protected virtual void OnPageLoaded(EventArgs e)
        { 
            PageLoaded?.Invoke(this, e);
        }

        protected virtual void OnTitleChanged(BrowserChangedEventArgs e)
        {
            TitleChanged?.Invoke(this, e);
        }

        /// <summary>
        /// Enable or disable toolbar buttons based on the state of the browser
        /// For example, when the toolbar is enabled, the back/forward buttons are enabled/disabled if the browser object "can go back/forward"
        /// </summary>
        private void SetToolBarEnabledState()
        {
            tsMain.Visible = _toolBarEnabled;
            if (_toolBarEnabled)
            {
                btnBack.Enabled = _browser.CanGoBack;
                btnForward.Enabled = _browser.CanGoForward;
            }
        }

        /// <summary>
        /// Set the web address displayed in the URL textbox in the toolbar 
        /// </summary>
        /// <param name="address">Web Address / URL</param>
        private void SetAddress(string address)
        {
            txtUrl.Text = address;
        }

        /// <summary>
        /// Navigate to web URL
        /// </summary>
        /// <param name="url">Web Address</param>
        public void LoadUrl(string url)
        {
            if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                _browser.Load(url);
        }

        /// <summary>
        /// Refresh the web URL that currently open/loaded in the browser
        /// </summary>
        public void RefreshPage()
        {
            _browser.Refresh();
        }

        /// <summary>
        /// Navigate back to previously loaded web URL
        /// </summary>
        public void GoBack()
        {
            if (_browser.CanGoBack)
                _browser.Back();
        }

        /// <summary>
        /// Navigate forward to previously loaded web URL 
        /// </summary>
        public void GoForward()
        {
            if (_browser.CanGoForward)
                _browser.Forward();
        }

        /// <summary>
        /// Print browser page via the Print Dialog
        /// </summary>
        public void Print()
        {
            _browser.Print();
        }

        private void Browser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            if (!e.IsLoading)
            {
                SetToolBarCallback cb = new SetToolBarCallback(SetToolBarEnabledState);
                Invoke(cb);
            }
        }

        private void Browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            SetAddressCallback cb = new SetAddressCallback(SetAddress);
            Invoke(cb, new object[] { e.Address });
        }

        private void Browser_TitleChanged(object sender, CefSharp.TitleChangedEventArgs e)
        {
            var customArgs = new BrowserChangedEventArgs()
            {
                Title = e.Title
            };
            OnTitleChanged(customArgs);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            GoForward();
        }

        private void txtUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                LoadUrl(txtUrl.Text);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshPage();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            LoadUrl(txtUrl.Text);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Print();
        }
    }
}
