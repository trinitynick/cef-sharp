﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cefsharpTester
{
    public class BrowserChangedEventArgs : EventArgs
    {
        public string Title { get; set; }
    }
}
